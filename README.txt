# This is a fork of olivere/elastic

The elastic subdirectory is only here to try to be more backward compatible with the original package.

# Usage

Replace your imports of `"gopkg.in/olivere/elastic.v2"` with `"gitlab.com/ofavre/olivere-elastic/elastic/v2"`.
Run: `go mod tidy`
